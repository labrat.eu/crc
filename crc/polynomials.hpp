#pragma once

#include <labrat/crc/crc.hpp>

namespace crc {
// NOLINTBEGIN

// Standard 32bit crc parameters.
using Crc32Ieee8023 = Crc<32, 0x04C11DB7, 0xffffffff, false, false, 0>;

// Standard 64bit crc parameters.
using Crc64Ecma182 = Crc<64, 0x42F0E1EBA9EA3693, 0x0000000000000000, false, false, 0x0000000000000000>;
using Crc64GoIso = Crc<64, 0x000000000000001B, 0xffffffffffffffff, true, true, 0xffffffffffffffff>;
using Crc64We = Crc<64, 0x42F0E1EBA9EA3693, 0xffffffffffffffff, false, false, 0xffffffffffffffff>;
using Crc64Xz = Crc<64, 0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, true, true, 0xFFFFFFFFFFFFFFFF>;

// NOLINTEND
}  // namespace crc
