#pragma once

#include <labrat/crc/table.hpp>

namespace crc {

template <u8 order, typename Unsigned<order>::Least polynomial, typename Unsigned<order>::Least initial_value = 0,
  bool input_reflected = false, bool output_reflected = false, typename Unsigned<order>::Least final_xor_value = 0>
using Crc = Table<order, polynomial, initial_value, input_reflected, output_reflected, final_xor_value>;

}  // namespace crc
