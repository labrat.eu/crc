#include <labrat/crc/polynomials.hpp>
#include <labrat/crc/table.hpp>
#include <labrat/standards/utils/cpp-utils/types.hpp>

#include <gtest/gtest.h>

namespace crc {

template <typename T>
struct CrcInfo {
  std::string name;
  T check_value;
};

template <typename CrcType, const CrcInfo<typename CrcType::Type> *&infos, uint info_index>
class Wrapper {
public:
  using Type = CrcType;
  const CrcInfo<typename CrcType::Type> &info = infos[info_index];
  uint index = info_index;
};

template <typename Wrapper>
class TableTest : public ::testing::Test {
protected:
  using Type = typename Wrapper::Type::Type;
  Wrapper wrapper;

  static constexpr typename Wrapper::Type table = typename Wrapper::Type();
};

TYPED_TEST_SUITE_P(TableTest);

TYPED_TEST_P(TableTest, checkOutput) {
  static const u8 data_length = 10;
  std::array<u8, data_length> data = {"123456789"};

  ASSERT_EQ(this->table.compute(data.data(), data.size() - 1), this->wrapper.info.check_value);

  crc::Table table = crc::Table<16, 0x8005, 0, true, true, 0>();
  printf("0x%04x\n", table.compute(data.data(), data.size() - 1));
  printf("0x%04x\n", table[1]);
}

// NOLINTBEGIN
const CrcInfo<u64> crc64_info[] = {
  {.name = "ecma182", .check_value = 0x6C40DF5F0B497347},
  {.name = "goIso", .check_value = 0xB90956C775A41001},
  {.name = "we", .check_value = 0x62EC59E3F1A4F00A},
  {.name = "xz", .check_value = 0x995DC9BBDF1939FA},
};
// NOLINTEND
// NOLINTBEGIN
struct CustomNameGenerator {
  template <typename T>
  static std::string GetName(int i) {
    return crc64_info[i].name;
  }
};
// NOLINTEND
const CrcInfo<u64> *crc64_info_pointer = crc64_info;

using CrcTypes = ::testing::Types<Wrapper<Crc64Ecma182, crc64_info_pointer, 0>, Wrapper<Crc64GoIso, crc64_info_pointer, 1>,
  Wrapper<Crc64We, crc64_info_pointer, 2>, Wrapper<Crc64Xz, crc64_info_pointer, 3>>;
REGISTER_TYPED_TEST_SUITE_P(TableTest, checkOutput);
INSTANTIATE_TYPED_TEST_SUITE_P(64, TableTest, CrcTypes, CustomNameGenerator);

}  // namespace crc
