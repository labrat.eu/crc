#pragma once

#include <labrat/standards/utils/cpp-utils/types.hpp>

#include <limits>

namespace crc {

// ToDo: document
template <u8 bits>
class Unsigned {
private:
  template <u8 type_index>
  struct TypeSelector {};

  template <>
  struct TypeSelector<4> {
    using Least = u8;
  };
  template <>
  struct TypeSelector<3> {
    using Least = u16;
  };
  template <>
  struct TypeSelector<2> {
    using Least = u32;
  };
  template <>
  struct TypeSelector<1> {
    using Least = u64;
  };

public:
  static_assert(bits <= 64);
  using Least = typename TypeSelector<static_cast<u8>(bits <= std::numeric_limits<u8>::digits)
    + static_cast<u8>(bits <= std::numeric_limits<u16>::digits) + static_cast<u8>(bits <= std::numeric_limits<u32>::digits)
    + static_cast<u8>(bits <= std::numeric_limits<u64>::digits)>::Least;
};

namespace internal {

template <typename T>
constexpr T alternatingBitmask(u8 width) {
  T mask = 0;
  for (u8 i = 0; i < std::numeric_limits<T>::digits; ++i) {
    mask |= (T)((i / width) % 2) << i;
  }
  return mask;
}

template <typename T>
constexpr T maskNBits(u8 width) {
  return ((((T)1 << (width - 1)) - 1) << 1) | 1;
}

template <typename T, u8 n>
constexpr T reflect(T data) {
  static_assert(sizeof(T) <= sizeof(u64));
  static_assert(std::numeric_limits<T>::digits >= n);

  for (u8 i = 1; i < std::numeric_limits<T>::digits; i <<= 1) {
    data = ((data & alternatingBitmask<T>(i)) >> i) | ((data & ~alternatingBitmask<T>(i)) << i);
  }

  return (data >> (std::numeric_limits<T>::digits - n));
}

static_assert(~(reflect<u64, 64>(alternatingBitmask<u64>(1 << 0)) ^ alternatingBitmask<u64>(1 << 0)) == 0);
static_assert(~(reflect<u64, 64>(alternatingBitmask<u64>(1 << 1)) ^ alternatingBitmask<u64>(1 << 1)) == 0);
static_assert(~(reflect<u64, 64>(alternatingBitmask<u64>(1 << 2)) ^ alternatingBitmask<u64>(1 << 2)) == 0);
static_assert(~(reflect<u64, 64>(alternatingBitmask<u64>(1 << 3)) ^ alternatingBitmask<u64>(1 << 3)) == 0);
static_assert(~(reflect<u64, 64>(alternatingBitmask<u64>(1 << 4)) ^ alternatingBitmask<u64>(1 << 4)) == 0);

template <typename T, u8 n>
constexpr T reciprocal(T data) {
  return ((internal::reflect<T, n>(data) << 1) & internal::maskNBits<T>(n)) | 1;
}

}  // namespace internal

}  // namespace crc
