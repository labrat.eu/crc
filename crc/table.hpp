/**
 * @file
 * @copyright Copyright (c) 2022 Leonard Moschcau
 * @author Leonard Moschcau (moschcau@disroot.org)
 * @brief Provides a table based crc implementation.
 *
 * @details This file provides a table based parameterized crc implementation. Take a look at @c Table for details on how to use this
 * implementation. The implementation is based on the following sources:
 *
 * - https://zlib.net/crc_v3.txt
 * - http://www.sunshine2k.de/articles/coding/crc/understanding_crc.html
 * - http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
 * - http://www.zorc.breitbandkatze.de/crc.html
 */

#pragma once

#include <labrat/crc/common.hpp>

#include <array>
#include <limits>

namespace crc {

/**
 * @brief @c Table is a parameterized table based crc implementation.
 *
 * @tparam order is the order of the @c polynomial in number of bits.
 * @tparam polynomial is the generator polynomial used for this instance of the crc table. See Table::model_polynomial for more information.
 * @tparam initial_value is used to initialize the crc value when computing the crc of some data.
 * @tparam input_reflected specifies whether data bytes should be reflected before a crc is calculated from them. The bytes are not actually
 * reflected instead a reversed crc table is generated.
 * @tparam output_reflected specifies whether the final crc value should be reflected.
 * @tparam final_xor_value is a value which is xored onto the crc value after all other operations.
 *
 * @details The constructor and all methods of @c Table are <a href="https://en.cppreference.com/w/cpp/language/constexpr">constexpr</a>.
 * This allows for compile time generation of the lookup table. While this might increase the compile time a tiny bit it is much saver than
 * to paste a precomputed crc table into the source as an array. For details on how to use the @c Table type have a look at the following
 * example.
 * @code {.cpp}
 * // The crc_table is computed at compile time based on the template parameters.
 * constexpr Table crc_table = Table<16, 0x1021, 0xffff, false, false, 0>();
 * uint8_t data[] = "123456789";
 * // Computes the crc of the data string and prints the result.
 * printf("crc of '%s' is 0x%04x.\n", data, crc_table.compute(data, 9));
 * @endcode
 *
 */
template <u8 order, typename Unsigned<order>::Least polynomial, typename Unsigned<order>::Least initial_value = 0,
  bool input_reflected = false, bool output_reflected = false, typename Unsigned<order>::Least final_xor_value = 0>
struct Table {
public:
  /**
   * @brief @c Type is the storage type used to hold the crc polynomial, the entries in the crc lookup table, and others. The @c Type is at
   * least large enough to hold the amount of bits specified by the @c order template parameter. For this the type @c Unsigned is used.
   */
  using Type = typename Unsigned<order>::Least;

  /**
   * @brief @c Reciprocal is a @c Table type specialized with template parameters so it represents the reciprocal of the original @c Table.
   */
  using Reciprocal =
    Table<order, internal::reciprocal<Type, order>(polynomial), initial_value, input_reflected, output_reflected, final_xor_value>;

  /**
   * @brief The @c model_polynomial field is the generator polynomial with which the crc table was constructed.
   *
   * @note The @c polynomial template parameter is a binary number where the bits are used as coefficients of the crc generator polynomial.
   * While there are multiple ways to represent a polynomial, this library uses the normal representation. To encode the following
   * polynomial @f$G@f$ one would need @f$n + 1@f$ bits, but most representations drop either the most or least significant bit as both
   * @f$x^{n}@f$ and @f$x^{0}@f$ are always @f$1@f$.
   * @f[G = x^{n} + x^{n - 1} + ... + x^{2} + x^{1} + x^{0}@f]
   * @f[G_{16CCITT} = x^{16} + x^{12} + x^{5} + 1@f]
   * The normal representation drops the most significant bit @f$x^{16}@f$ and represents @f$G_{16CCITT}@f$ as @c 0x1021.
   * In contrast the Koopman representation drops the least significant bit @f$x^{0}@f$ and represents @f$G_{16CCITT}@f$ as @c 0x8810.
   */
  static constexpr Type model_polynomial = polynomial;

  /**
   * @brief The @c model_initial_value field is the value used to initialize the crc when computing the crc of some data.
   */
  static constexpr Type model_initial_value = initial_value;

  /**
   * @brief The @c model_input_reflected field specifies whether data bytes are reflected before a crc is calculated from them.
   *
   * @note The bytes are not actually reflected, instead a reverse crc table is generated. This option is usefull when dealing with hardware
   * systems that send data bytes in reverse bit order. An example of this is UART. Have a look at the references in the file @c table.hpp
   * for a more detailed explanation of all parameters.
   */
  static constexpr bool model_input_reflected = input_reflected;

  /**
   * @brief The @c model_output_reflected field specifies whether the final crc value should be reflected.
   */
  static constexpr bool model_output_reflected = output_reflected;

  /**
   * @brief The @c model_final_xor_value field is the value which is xored onto the crc value after all other operations.
   */
  static constexpr Type model_final_xor_value = final_xor_value;

  // Assert that the polynomial order is at least 1 but not greater than the biggest usable unsigned integer data type.
  static_assert(order >= 1 && order <= std::numeric_limits<u64>::digits);
  // Assert that for none of the template parameter values a bit outside the crc order is set.
  static_assert(polynomial == (polynomial & internal::maskNBits<Type>(order)));
  static_assert(initial_value == (initial_value & internal::maskNBits<Type>(order)));
  static_assert(final_xor_value == (final_xor_value & internal::maskNBits<Type>(order)));

  /**
   * @brief Construct a @c Table instance based on the template parameters.
   *
   * @details Have a look at the @c Table type for more information about the template parameters.
   */
  explicit constexpr Table() : table() {
    // iterate over all possible input byte values 0 - 255
    for (u16 divident = 0; divident < std::numeric_limits<u8>::max() + 1; ++divident) {
      u8 div = (input_reflected ? internal::reflect<Type, 8>(divident) : divident);

      // move divident byte into MSByte of crc
      Type current_byte = (order >= bits_in_byte) ? (Type)div << (order - bits_in_byte) : (Type)div >> (bits_in_byte - order);

      // shift crc to the left eight times
      for (u8 bit = 0; bit < bits_in_byte; ++bit) {
        bool first_bit = (current_byte & (Type)1 << (order - 1)) != 0;
        current_byte <<= 1;
        if constexpr (order < bits_in_byte) {
          if ((bits_in_byte - order - (bit + 1)) >= 0) {
            current_byte ^= (Type)(div >> (bits_in_byte - order - (bit + 1))) & 1;
          }
        }
        // if the MSBit is 1 the polynomial is xor-ed
        if (first_bit) {
          current_byte ^= polynomial;
        }
      }

      if constexpr (input_reflected) {
        current_byte = internal::reflect<Type, order>(current_byte);
      }

      // save the computed crc of the current byte into the table
      table[divident] = current_byte & internal::maskNBits<Type>(order);
    }
  }

  /**
   * @brief This operator allows access to the underlying crc lookup table.
   *
   * @param[in] index is the index of the element which should returned.
   * @return The table element at @c index.
   */
  Type operator[](u8 index) const {
    return table[index];
  }

  /**
   * @brief Computes a crc using this @c Table and its template parameters.
   *
   * @param[in] bytes is a pointer to a sequence of bytes of which a crc should be calculated.
   * @param[in] count is the number of bytes to calculate the crc from.
   * @return The calculated crc value.
   *
   * @note @c Table uses a direct implementation, as a result the parameter @c count should include only the data and no additional zero
   * bytes which might be added to save the crc value afterwards. To check the crc of some data with appended crc @c count should
   * additionally include the bytes used to save the crc.
   */
  constexpr Type compute(const u8 *bytes, u64 count) const {
    Type crc = (input_reflected) ? internal::reflect<Type, order>(initial_value) : initial_value;
    u8 position = 0;
    [[likely]] for (u64 i = 0; i < count; ++i) {
      // xor-in next input byte into MSB of crc, that's our new intermediate
      // divident.
      if constexpr (input_reflected) {
        position = (u8)((crc & std::numeric_limits<u8>::max()) ^ bytes[i]);
        crc = (Type)((crc >> bits_in_byte) ^ table[position]);
      } else {
        if constexpr (order >= bits_in_byte) {
          position = (u8)(((crc >> (order - bits_in_byte)) & std::numeric_limits<u8>::max()) ^ bytes[i]);
        } else {
          position = (u8)(crc << (bits_in_byte - order) ^ bytes[i]);
        }
        crc = (Type)((crc << bits_in_byte) ^ table[position]);
      }
    }

    if constexpr ((output_reflected && !input_reflected) || (!output_reflected && input_reflected)) {
      crc = internal::reflect<Type, order>(crc);
    }

    crc ^= final_xor_value;

    return internal::maskNBits<Type>(order) & crc;
  }

protected:
  /**
   * @brief The crc lookup table.
   */
  std::array<Type, std::numeric_limits<u8>::max() + 1> table;

  /**
   * @brief A shorthand constant which holds the number of bits in a byte.
   */
  static const constexpr u8 bits_in_byte = std::numeric_limits<u8>::digits;
};

}  // namespace crc
