#!/bin/sh

# Script to generate CI variables.

# Initialize possibly uninitialized variables.
CPP_NO_DOCKER_CACHE=${CPP_NO_DOCKER_CACHE:-'0'}

# Prepare the environment.
if [ $CPP_NO_DOCKER_CACHE ]; then
  EXTRA_ARGS="$EXTRA_ARGS --no-cache"
fi

# Change path.
cd dependencies/ubuntu

# Try to pull the last commits image if the dependencies have not changed.
if [ $CPP_DEPENDENCIES_UNCHANGED_UBUNTU -ne 0 ] && [ $CPP_NO_DOCKER_CACHE -eq 0 ]; then
  # Unset the build image flag if the pull was successful.
  BUILD_IMAGE=0
  docker pull $CI_REGISTRY_IMAGE:build-ubuntu-$CI_LAST_COMMIT_SHORT_SHA || BUILD_IMAGE=$?
else
  # Set the build image flag.
  BUILD_IMAGE=1
fi

if [ $BUILD_IMAGE -ne 0 ]; then
  # Build the docker image from scratch.
  docker build $EXTRA_ARGS -t $CI_REGISTRY_IMAGE:build-ubuntu-$CI_COMMIT_SHORT_SHA .
else
  # Reuse the old image.
  docker tag $CI_REGISTRY_IMAGE:build-ubuntu-$CI_LAST_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:build-ubuntu-$CI_COMMIT_SHORT_SHA
fi

# Push the image.
docker push $CI_REGISTRY_IMAGE:build-ubuntu-$CI_COMMIT_SHORT_SHA

exit 0
