#!/bin/sh

# Script to generate CI variables.

# Get the last commit.
export CI_LAST_COMMIT_SHORT_SHA=$(git rev-parse HEAD^1 | cut -c 1-8)

# Check if the dependency scripts have been changed since the last commit.
git diff --name-only HEAD HEAD~1 | grep -qE ^dependencies/ubuntu/install-build-dependencies.sh$
export CPP_DEPENDENCIES_UNCHANGED_UBUNTU=$?

# Write the variables to a file.
echo "CI_LAST_COMMIT_SHORT_SHA=$CI_LAST_COMMIT_SHORT_SHA" >> build.env
echo "CPP_DEPENDENCIES_UNCHANGED_UBUNTU=$CPP_DEPENDENCIES_UNCHANGED_UBUNTU" >> build.env

exit 0
