# C/C++ template repository

Use this repository as a way to initialize your C/C++ projects.


## Documentation

You can find the latest Doxygen documentation [here](https://labrat.eu.gitlab.io/standards/labrat/cpp).


## Running the application

### Docker

If you want to run the application in docker containers, you can either pull the latest build from the registry or build the docker image yourself.

Note: To build the image you first have to [build the application](## Building).

The `<repo-name>` will most likey be `registry.gitlab.com/labrat.eu/standards/labrat/cpp`.
```bash
docker build -t <repo-name> .
```

To start your containers you can make use of a docker-compose file. Just replace `<n>` with the number of instances you want to create.
You can either use the existing docker-compose file delivered with your downloaded build or generate the file yourself (In that case you might want to install some dependencies first).
```bash
cd build
cmake .. -DCPP_DOCKER_IMAGE=<repo-name>
docker-compose up -d --scale dd=<n>
```


## Installing dependencies

Note: If you want to cross-compile the project you may need to install further packages.

### Automatic install for supported distros

Automatic install scripts are supported for the following distros:
- Ubuntu (including similar distros like debian or Linux-Mint)

To automatically install the required dependencies you can simply execute the following script:
```bash
sudo ./dependencies/<distro>/install-run-dependencies.sh
```

Or if you want to build the project:
```bash
sudo ./dependencies/<distro>/install-build-dependencies.sh
```

### Manual install

There are currently no runtime dependencies.

To build the project you will need to also install the following packages manually:
- cmake (>=3.18.0)
- clang (>=14)
- clangd (>=14)
- lld (>=14)
- llvm-dev (>=14)
- libstdc++ (>=11)
- make (>=4.2.0)

Depending on your needs you might need to install the following packages manually as well:
- clang-tidy (>=14)
- clang-format (>=14)
- docker.io (>=20.10.0)
- doxygen (>=1.9)
- gtest (>=1.11.0)
- git (>=2.34.0)
- graphviz (>=2.42.0)

### Using a docker image for building

Alternatively you can also use a docker image which already has the dependencies installed.
To build the docker image use the following commands:
```bash
cd dependencies/<distro>
docker build -t <repo-name>/build-<distro> .
```
You can also browse existing build images for the project in the registry.

## Initializing the git repository

Please initialize the repository by executing the following command.
```bash
./tools/git-init.sh
```


## Building

Note: Before you can build the project you first need to [install](## Installing dependencies) the dependencies.

You can build the project by creating a build directory and configuring it with CMake.
Please select the appropriate toolchain through the `CMAKE_TOOLCHAIN_FILE` option as follows.
```bash
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../toolchains/<compiler>/<triple>.cmake
make -j${nproc}
```


## Installing

You can install the project by executing the following command in the build directory.
```bash
sudo make install
```
