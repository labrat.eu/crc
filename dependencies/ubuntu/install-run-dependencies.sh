#!/bin/sh

# Install all required run dependencies for Ubuntu.

# Exit on any failure.
set -e

# Install apt packages.
apt-get update -qq
apt-get install -qq -y
