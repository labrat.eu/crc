#!/bin/sh

# Install all required build dependencies for Ubuntu.

# Exit on any failure.
set -e

# Change working directory to script directory.
dir=$(dirname $0)
cd $dir
absdir=$(pwd)

# Gather version information.
if [ ! -f /etc/lsb-release ]; then
  echo "File lsb-release is missing."
  exit 1
fi
. /etc/lsb-release
distro=${DISTRIB_ID}
version=${DISTRIB_RELEASE}
distro_version="${distro}_${version}"

# Apt based packages.

# Install basic apt packages.
apt-get update -qq
apt-get install -y wget software-properties-common

# CMake.
case "$distro_version" in
  Ubuntu_16.04 ) cmake_repo="https://apt.kitware.com/ubuntu/ xenial main" ;;
  Ubuntu_18.04 ) cmake_repo="https://apt.kitware.com/ubuntu/ bionic main" ;;
  Ubuntu_18.10 ) cmake_repo="https://apt.kitware.com/ubuntu/ bionic main" ;;
  Ubuntu_19.04 ) cmake_repo="https://apt.kitware.com/ubuntu/ bionic main" ;;
  Ubuntu_19.10 ) cmake_repo="https://apt.kitware.com/ubuntu/ bionic main" ;;
  Ubuntu_20.04 ) cmake_repo="https://apt.kitware.com/ubuntu/ focal main" ;;
  Ubuntu_21.04 ) cmake_repo="https://apt.kitware.com/ubuntu/ focal main" ;;
  Ubuntu_21.10 ) cmake_repo="https://apt.kitware.com/ubuntu/ focal main" ;;
  Ubuntu_22.04 ) cmake_repo="https://apt.kitware.com/ubuntu/ focal main" ;;
  * ) echo "Distribution is not supported by this script (${distro_version})."; exit 1
esac

wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] ${cmake_repo}" | tee /etc/apt/sources.list.d/kitware.list >/dev/null

# Clang.
case "$distro_version" in
  Ubuntu_16.04 ) clang_repo="https://apt.llvm.org/xenial/ llvm-toolchain-xenial main" ;;
  Ubuntu_18.04 ) clang_repo="https://apt.llvm.org/bionic/ llvm-toolchain-bionic main" ;;
  Ubuntu_18.10 ) clang_repo="https://apt.llvm.org/cosmic/ llvm-toolchain-cosmic main" ;;
  Ubuntu_19.04 ) clang_repo="https://apt.llvm.org/disco/ llvm-toolchain-disco main" ;;
  Ubuntu_19.10 ) clang_repo="https://apt.llvm.org/eoan/ llvm-toolchain-eoan main" ;;
  Ubuntu_20.04 ) clang_repo="https://apt.llvm.org/focal/ llvm-toolchain-focal main" ;;
  Ubuntu_21.04 ) clang_repo="https://apt.llvm.org/hirsute/ llvm-toolchain-hirsute main" ;;
  Ubuntu_21.10 ) clang_repo="https://apt.llvm.org/impish/ llvm-toolchain-impish main" ;;
  Ubuntu_22.04 ) clang_repo="http://apt.llvm.org/jammy/ llvm-toolchain-jammy main" ;;
  * ) echo "Distribution is not supported by this script (${distro_version})."; exit 1
esac

wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/llvm-archive-keyring.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/llvm-archive-keyring.gpg] ${clang_repo}" | tee /etc/apt/sources.list.d/llvm.list >/dev/null

# Install apt packages.
apt-get update -qq
apt-get install -qq -y clang clangd clang-tidy clang-format cmake docker.io doxygen git graphviz libc++-11-dev libgtest-dev libgmock-dev lld llvm-dev make pkg-config

# Manual installations.
export CC=clang
export CXX=clang++
mkdir .tmp
cd .tmp

# Cleanup.
cd ..
rm -rf .tmp
