cmake_minimum_required(VERSION 3.18.0)

# Set project name
set(LOCAL_PROJECT_NAME crc)
set(LOCAL_PROJECT_NAMESPACE labrat)
set(LOCAL_PROJECT_DESCRIPTION "C++ crc header library")

string(TOUPPER ${LOCAL_PROJECT_NAME} LOCAL_PROJECT_NAME_CAPS)
string(REPLACE "-" "_" LOCAL_PROJECT_NAME_CAPS ${LOCAL_PROJECT_NAME_CAPS})

# Set project root name and path.
set(LOCAL_PROJECT_ROOT_NAME labrat)
set(LOCAL_PROJECT_PATH ${LOCAL_PROJECT_ROOT_NAME}/${LOCAL_PROJECT_NAME})
set(LOCAL_PROJECT_FULL_NAME ${LOCAL_PROJECT_ROOT_NAME}_${LOCAL_PROJECT_NAME})
