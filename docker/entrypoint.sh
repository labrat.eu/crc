#!/bin/sh

# Entrypoint script for the cpp docker image.

# Usage string function.
usage() {
  echo "Usage: $0 [options] [args passed onto cpp]"
  echo "Entrypoint script for cpp."
  echo
  echo "Options:"
  echo "  -v Display version of this build and exit."
  echo "  -h Display this help page and exit."

  exit 0
}

# Process command line options.
while getopts 'vh' flag; do
  case "$flag" in
  v) /usr/bin/cpp -v; exit $? ;;
  h) usage ;;
  esac
done

# Run cpp with passed on arguments.
/usr/bin/cpp -c $@
